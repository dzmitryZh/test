import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2, OnDestroy
} from '@angular/core';
import { fromEvent, from, combineLatest, Observable, of, forkJoin } from 'rxjs';
import { map, pairwise, switchMap, takeUntil, combineAll, take, catchError } from 'rxjs/operators';
import { WSService } from '../services/ws.service';
import { Coordinates } from '../types/coords';
import { Dimension, WSShape } from '../types/shape';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('board', { static: true }) boardRef: ElementRef;
  private board: HTMLDivElement;
  private canvas: HTMLCanvasElement;
  private context: CanvasRenderingContext2D;
  private currentShapeStore: Coordinates[] = [];

  public colors = ['red', 'green', 'blue', 'black'];
  public currentColor = 'black';

  public canvases: HTMLCanvasElement[] = [];

  constructor(private renderer: Renderer2, private ws: WSService) {}

  ngOnInit(): void {
    this.board = this.boardRef.nativeElement as HTMLDivElement;
    this.initStream();
    this.ws.connect();
    this.ws.onNewMessage.subscribe(shape => {
      this.createCanvas();
      const dimensions = this.getDimensions(shape.coordinates);
      this.renderer.setStyle(this.canvas, 'position', 'absolute');
      this.setCanvasSize(dimensions);
      this.renderCanvas(shape);
    });
  }

  public changeColor(color: string): void {
    this.currentColor = color;
  }

  private getDimensions(coords: Coordinates[]): Dimension {
    const coordsX = coords.map(item => item.from.x);
    const coordsY = coords.map(item => item.from.y);
    const minX = Math.min(...coordsX);
    const maxX = Math.max(...coordsX);
    const minY = Math.min(...coordsY);
    const maxY = Math.max(...coordsY);
    return {
      width: maxX - minX,
      height: maxY - minY
    };
  }

  private renderCanvas(shape: WSShape): void {
    const minX = Math.min(...shape.coordinates.map(item => item.from.x));
    const minY = Math.min(...shape.coordinates.map(item => item.from.y));
    shape.coordinates.map(item => {
      this.context.strokeStyle = shape.color;
      this.context.beginPath();
      this.context.moveTo(item.from.x - minX, item.from.y - minY);
      this.context.lineTo(item.to.x - minX, item.to.y - minY);
      this.context.stroke();
    });
    this.renderer.setStyle(this.canvas, 'transform', `translate(${minX}px, ${minY}px)`);
  }

  private createCanvas(): void {
    this.canvas = this.renderer.createElement('canvas');
    this.renderer.addClass(this.canvas, 'board__item');
    this.context = this.canvas.getContext('2d');
    const rect = this.board.getBoundingClientRect();
    this.context.scale(1, 1);
    this.setCanvasSize(rect);
    this.renderer.appendChild(this.board, this.canvas);
  }

  private initStream(): void {
    const mouseMove$ = fromEvent(this.board, 'mousemove');
    const mouseDown$ = fromEvent(this.board, 'mousedown')
      .pipe(map(() => this.createCanvas()));
    const mouseUp$ = fromEvent(this.board, 'mouseup');

    const stream$ = mouseDown$.pipe(
      switchMap(() => {
        return mouseMove$.pipe(
          map((event: MouseEvent) => {
            return {
              x: event.clientX,
              y: event.clientY,
            };
          }),
          pairwise(),
          takeUntil(mouseUp$),
        );
      })
    );

    stream$.subscribe(([from, to]) => {
      const coords: Coordinates = { from, to };
      this.context.strokeStyle = this.currentColor;
      this.context.beginPath();
      this.context.moveTo(from.x, from.y);
      this.context.lineTo(to.x, to.y);
      this.context.stroke();
      this.currentShapeStore.push(coords);
    });

    mouseUp$.subscribe(() => {
      const shape: WSShape = {
        color: this.currentColor,
        coordinates: this.currentShapeStore,
      };
      const dimensions = this.getDimensions(shape.coordinates);
      this.renderer.setStyle(this.canvas, 'position', 'absolute');
      this.setCanvasSize(dimensions);
      this.renderCanvas(shape);
      this.ws.send(shape);
      this.currentShapeStore = [];
    });
  }

  private setCanvasSize({ width, height }): void {
    this.renderer.setProperty(this.canvas, 'width', width);
    this.renderer.setProperty(this.canvas, 'height', height);
  }

  public ngOnDestroy() {
    this.ws.close();
  }
}
