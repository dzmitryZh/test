export interface Coordinates {
  from: Point;
  to: Point;
}

export interface Point {
  x: number;
  y: number;
}
